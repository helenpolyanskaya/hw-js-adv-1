//   1) Прототипне наслідування це коли обʼєкт може успадкувати властивості та методи іншого обʼєекта.
//   2) super визиває батькивский конструктор.


class Employee  {

    constructor(name, age, salary){
        this.name = name;
        this._age = age;
        this.salary = +salary
    }   

    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }

    get salary() {
        return this._salary;
    }

    set name(valueName) {
       this._name = valueName;
    }

    set age(valueAge) {
        this._age = valueAge;
    }

    set salary(valueSalary) {
        this._salary = valueSalary;
    }
}

class Programmer extends Employee {
    
    constructor(name, age, salary,lang){
        super(name, age, salary)
        this.lang = lang
    }

    get salary() {
        return this._salary * 3;
    }  

    set salary(valueSalary) {
        this._salary = valueSalary;
    }
}

let elena = new Employee ("Elena", "36", 200);
console.log(elena);
let viktor = new Programmer ("Viktor", "38", 3800, ['ua', 'en', 'es']);
console.log(viktor);
let illiya = new Programmer ("Illiya", "33", 2500, ['ua', 'en', 'fr']);
console.log(illiya);
let oksana = new Programmer ("Oksana", "39", 420, ['en', 'ua']);
console.log(oksana);

